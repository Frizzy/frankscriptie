% THIS IS SIGPROC-SP.TEX - VERSION 3.1
% WORKS WITH V3.2SP OF ACM_PROC_ARTICLE-SP.CLS
% APRIL 2009
%
% It is an example file showing how to use the 'acm_proc_article-sp.cls' V3.2SP
% LaTeX2e document class file for Conference Proceedings submissions.
% ----------------------------------------------------------------------------------------------------------------
% This .tex file (and associated .cls V3.2SP) *DOES NOT* produce:
%       1) The Permission Statement
%       2) The Conference (location) Info information
%       3) The Copyright Line with ACM data
%       4) Page numbering
% ---------------------------------------------------------------------------------------------------------------
% It is an example which *does* use the .bib file (from which the .bbl file
% is produced).
% REMEMBER HOWEVER: After having produced the .bbl file,
% and prior to final submission,
% you need to 'insert'  your .bbl file into your source .tex file so as to provide
% ONE 'self-contained' source file.
%
% Questions regarding SIGS should be sent to
% Adrienne Griscti ---> griscti@acm.org
%
% Questions/suggestions regarding the guidelines, .tex and .cls files, etc. to
% Gerald Murray ---> murray@hq.acm.org
%
% For tracking purposes - this is V3.1SP - APRIL 2009

\documentclass{acm_proc_article-sp}

\begin{document}

\title{ {\ttlit Research Design:} predicting an author's age, gender and personality by analysing twitter messages
\subtitle{[Extended Abstract]}}
%
% You need the command \numberofauthors to handle the 'placement
% and alignment' of the authors beneath the title.
%
% For aesthetic reasons, we recommend 'three authors at a time'
% i.e. three 'name/affiliation blocks' be placed beneath the title.
%
% NOTE: You are NOT restricted in how many 'rows' of
% "name/affiliations" may appear. We just ask that you restrict
% the number of 'columns' to three.
%
% Because of the available 'opening page real-estate'
% we ask you to refrain from putting more than six authors
% (two rows with three columns) beneath the article title.
% More than six makes the first-page appear very cluttered indeed.
%
% Use the \alignauthor commands to handle the names
% and affiliations for an 'aesthetic maximum' of six authors.
% Add names, affiliations, addresses for
% the seventh etc. author(s) as the argument for the
% \additionalauthors command.
% These 'additional authors' will be output/set for you
% without further effort on your part as the last section in
% the body of your article BEFORE References or any Appendices.

\numberofauthors{2} %  in this sample file, there are a *total*
% of EIGHT authors. SIX appear on the 'first-page' (for formatting
% reasons) and the remaining two appear in the \additionalauthors section.
%
\author{
% You can go ahead and credit any number of authors here,
% e.g. one 'row of three' or two rows (consisting of one row of three
% and a second row of one, two or three).
%
% The command \alignauthor (no curly braces needed) should
% precede each author name, affiliation/snail-mail address and
% e-mail address. Additionally, tag each line of
% affiliation/address with \affaddr, and tag the
% e-mail address with \email.
%
% 1st. author
\alignauthor
Frank Houweling\\
       \affaddr{University of Amsterdam}\\
       \affaddr{Master Information Studies}\\
       \affaddr{Human Centered Multimedia}\\
       \email{frank.houweling@student.uva.nl}
% 2nd. author
\alignauthor
Maarten Marx\titlenote{Thesis Supervisor}\\
       \affaddr{University of Amsterdam}\\
       \affaddr{Informatics Institute}\\
       \affaddr{ILPS}\\
       \email{maartenmarx@uva.nl}
}

\maketitle
\begin{abstract}
In the PAN 2015 profiling task the goal is set to predict an author's age, gender and the 5 personality traits purely on the content of their send tweets. Also, these authors write in different languages: English, Spanish, Italian and Dutch. To achieve this, the subprofile method described by \cite{lopezusing} is used in combination with different features.
\end{abstract}

% A category with the (minimum) three required fields
\category{H.4}{Information Systems Applications}{Miscellaneous}
%A category including the fourth, optional field follows...
\category{D.2.8}{Software Engineering}{Metrics}[complexity measures, performance measures]

\terms{Author Profiling, Text Analysis, Data Mining}


\section{Introduction}
In different fields the interest in text analysis tools is growing. Author profiling, which goal it is to, given a piece of plain text, predict different demographic features of the author, is an example of an interesting application of text analysis. The demographic features that result from author profiling can be used to build a demographic description of the author (an author profile) which promises many possibilities for business intelligence, criminal law, computer forensics and more.\par
For example, in the case of business intelligence and marketing, these profiles can be used to form a generalized view of the demographics of loyal customers, the people that dislike a particular product or other interesting groups. The business can use that information to design new products for the groups of people they do not yet reach, or better target advertisements on people with the same demographics as their existing loyal customers. As previously mentioned, author profiling can also be of great interest in the field of computer forensics. In many cases, it can be of interest to forensic specialists to determine the identity of the author of a piece of text. When the author is not directly available, or different people are seen as potential authors of the piece of text, a demographic profile of the author can help in narrowing the scope of the search.\par
Previous work in the field of author profiling was often initiated by the PAN evaluation lab which in 2013 and 2014 was a part of the yearly CLEF congress. Also in 2015 PAN will be organized, and a author profiling task is available. Achieving the goal posed by the task will be the general focus of this master thesis. Because this evaluation task has been part of PAN for some previous years, the method seems obvious. This is however not the case. In PAN 2015, many changes have been made in the task description.\par
The PAN 2015 task consist of predicting age, gender and personality traits from twitter messages or tweets. The method used should be applicable on texts written in different languages, and because of that the to be used corpus is compiled from English, Spanish, Italian and Dutch. Based on this task, the following research question is defined: "How can age, gender and personality traits be derived from short social texts in a multilingual setting?"


\section{Related Work}
When designing a method to perform author profiling on the different representations mentioned earlier (age, gender and the personality traits), it is generally a good idea to start with looking at previous work, and with that to stand on the shoulders of giants. Because no literature is available that focuses on all these representations at once, multiple methods will have to be combined to achieve a good result for both the age and gender, and the personality traits. Also, it is important to keep in mind that the features and algorithms described should work in a multilingual setting, where only short social status updates are used.

\subsection{Features}
In previous work that was focused on determining age and gender, the task was solved as a supervised classification problem. [\cite{rangel2013overview} A large diversity in used features can be found. Rangel et al. (2013) \nocite{rangel2013overview} wrote an overview of the approaches used by the participants of the PAN 2013 author profiling task. Of these participants, most used 	different stylistic features such as punctuation marks, quotations and so on. Also frequently used are content features such as bag of words, TF-IDF or dictionary-based words, often combined with n-grams of words and/or characters. Next to these methods, there were often document features that gave extra focus on named entities, sentiment words, emotion words or slang.\par
Approaches that focused on classifying authors on personality used a completely different set of features. For example Farnadi et al. (2013) \nocite{farnadi2013recognising} researched how to classify authors on personality traits, based on their facebook status updates. Twitter messages and facebook status updates are of course not the same thing, but because they both consist of relatively short but highly social content, it is very likely that the same approach can be used.\par
	Farnadi et al. used the LIWC featureset \cite{pennebaker1999linguistic} as a basis, which includes features related to psychological processes and personal concerns. They extended this set with different social network features such as network size, betweenness, density etc. as described by O’Malley and Marsden (2008)\nocite{o2008analysis}. These were calculated using the facebook friend network. These cannot be used in exactly the same way on twitter as they were in the research of Fernadi et al. because there is no comparable functionality to add friends in Twitter, but Ediger et al. (2010) \nocite{ediger2010massive} describe how the same network measures can be derived using mentions of authors in twitter messages.\par
Finally, Fernadi et al. also used time-related features. These time-related features describe the timeframes in which most updates are posted, and the frequency in which the author published new updates. They describe how all the features described above significantly contribute to the classification of one ore more personality traits.

\subsection{Language dependency}
In the limited amount of research on author profiling in different languages then english was found that there are differences in the feature spaces of texts in different languages \cite{estival2007tat}. Because the research question identifies the need for an approach that is applicable in a multilingual setting, it is important to create an overview of language dependency of different features.

\begin{description}
  \item[Content features]
  In the research of Estival et al. Arabic texts where analyzed in a author profiling setting. These texts had very different features then an english text would have, because of the different word sizes and character sets. When working with languages that use the latin character set, the first difference in features might not be a big problem, but differences in word lengths might be found and should be compensated for.
  \item{Stylistic features}
  Different languages have different norms for the use of interpunction and emoticons. For example, in Spanish it is the norm to use two question marks when writing down a question \cite{houston2013shady}.
\end{description}
The other two categories of features discussed: time-based features and social network features, are not likely to be dependent on language.

\subsection{Algorithms}
Finding an algorithm that performs well in this setting is far from obvious. Fortunately, there is already a substantial body of literature on the topic. Most research at this point is focused on determining the age and gender of the author. In existing research, many efforts apply the same algorithm in achieving author profiling: supervised machine learning techniques, and then in particular linear svm's, are used to map the authors to the different profiles that are determined for the profiling task, using vectors with a dimension for every representation. The classification process is repeated separately for every representation. This concept is called Second Order Attributes (SOA) \cite{lopez2012new}.\par
SOA has shown to be pretty successful, but has one large shortcoming. The approach assumes that there are direct relations to features and profiles. For example, terms related to horses are directly related to the class female. While this represents a generalized view of the average person writing about horses, it incorrectly classifies every male author writing about the subject. \par
In their entry for PAN 2014, L{\'o}pez-Monroy et al. (2014) applied a slightly different method than the one we described before \cite{lopezusing}. The method L{\'o}pez-Monroy et al. describe is an extension to the Second Order Attributes method, where it does not only use the eventual profiles in which an author can be profiled, but the method also generates subprofiles. These subprofiles can then be used to explain clusters of  the authors who do not match the generalized profile; for example the males who do write about horses. This method has proven to be very effective, where the model of L{\'o}pez-Monroy et al. was the one with the highest effectiveness of all entries for PAN 2014. Because of this reason, this method will also be used as the basis for this thesis.\par
But this method might not be sufficient. In this research, not only age and gender are classified, but the algorithm should also correctly classify authors based on personality traits. To be able to do this, adjustments might have to be made.

\section{Methodology}
The approach presented in this paper consist of three steps. Firstly, the free texts of all messages in the training set are transformed in vectors of features. Secondly, these training features are used to generate subprofiles using the subprofile generation algorithm of L{\'o}pez-Monroy et al. (2014). Lastly, the authors in the test set are classified upon the subprofiles using a simple SVM.

\subsection{Data Selection}
The corpus used in this research is the one provided by PAN 2015. The corpus exists of x [unknown, will be known in some weeks] twitter messages in English, Spanish, Italian and Dutch. These messages are categorized by author. The authors in the corpus are annotated with the respective age category (18-24, 25-34, 35-49, 50-64 or 65-xx), the gender and a score between -0.5 and 0.5 for each of the five personality traits described by \cite{costa1992revised}

\subsection{Feature extraction}
Because of the wide range of representations on which author profiling needs to be performed, a selection of features from the literature review are used.

\begin{description}
  \item[Content features] A selection of N-grams ($n >= 1 <= 4$) is used to generate an overall view of the contents of the author's texts. These are of course normalized using TF-IDF. \cite{ramos2003using} Next to this, swear words, slang and words that express emotions are emphasized by seeing them as a separate feature. Also, the total length of the texts is used.
  \item[Stylistic features] Two stylistic features are taken into account. Firstly, punctuation marks (questionmarks, kommas etc.) are counted, and these counts are used as features. Secondly, the amount of emoticons is used as an feature. 
  \item[Time-based features] Two  time-based features are used: the average amount of messages for every time frame (morning: 05:00 till 12:00, afternoon: 12:00 18:00, evening: 18:00 till 23:00 and night 23:00 till 05:00) and the average amount of messages per day.
  \item[Social Network Features] Lastly, a set of social network features are used as features, and can help to determine an author's personality \cite{farnadi2013recognising}.
\end{description}
  
\subsection{Algorithm}
Because of the improved result towards the frequently-used Second Order Attributes, the subprofile method described by {\'o}pez-Monroy et al (2014) is also used in this research. It could be summarized as the following: \par
Firstly, for each person in the training set the features described in the previous section are calculated. The features of these texts are combined to form one vector per author.\par
Secondly, for each representation (age, gender, personality), we look at groups of authors in the training set with the same value, and cluster these authors based on their full feature set. For clustering, K-means is used. For K-means, $K = 5$.\par
The result of this clustering is a group of clusters that is five times larger then simply the amount of representations multiplied with the amount of possibilities.\par
Based on these clusters, a SVM classifier is constructed. This classifier classifies new authors in one of these clusters, in stead of the normal classes. Eventually, the real class of the resulting cluster is determined and used as a result.\par

\subsection{Evaluation}
To achieve an optimal performance at the PAN 2015 task, the same evaluation method is used. This means that there is a separate evaluation for the age and gender, and for the personality traits.\par
For both the age and the gender, the simple accuracy is used as the evaluation method. In the case of personality traits, a more complex evaluation method is used, namely the average root mean squared error. This is because the same will be used for the evaluation at the PAN congress, and the system should be optimized to perform well at PAN.

\section{Planning}
The timespan of this project is from the 30th of March till the 26th of June. In this relative short time, a little over three months, the goal is to finish with an author profiling system that can compete with other competitors at PAN 2015. To achieve this, the following time planning is set:

\begin{center}
    \begin{tabular}{ | l | p{5cm} |}
    \hline
    Period & Planning \\ \hline
    Before 30th (March) & Finish literature review and methodology \\ \hline
    First 2 weeks (April) & Data preparation and content-, stylistic- and time features \\ \hline
    Last 2 weeks (April) & Social network features \\ \hline
    First 2 weeks (May) & Building feature vectors and clustering sub-profile clusters \\ \hline
    Last 2 weeks (May) & Constructing linear SVM \\ \hline
    1st week (June) & Improving found method \\ \hline
    2 - 3th week (June) & Finishing report \\ \hline
    Last week (June) & Free for planning \\
    \hline
    \end{tabular}
\end{center}

\subsection{Deadlines}
To be able to submit this paper to the PAN 2015 conference, the resulting software should be finished and submitted on April the 15th, or earlier. The resulting paper has to be submitted before May the 24th.

\subsection{Flexibility and 'Plan B'}
Of course there is room in this planning for changes and drawbacks. Firstly, the last week is free for planning, when extra time is necessary for any of the parts of the research. Secondly, the improvement of the found method in the first week of June could be skipped. \\
As shown in the table, to make the project fit the short time span, the literature review and methodology are completely finished before the beginning of the project.

%\end{document}  % This is where a 'short' article might terminate

%ACKNOWLEDGMENTS are optional
% \section{Acknowledgments}
% This section is optional; it is a location for you
% to acknowledge grants, funding, editing assistance and
% what have you.  In the present case, for example, the
% authors would like to thank Gerald Murray of ACM for
% his help in codifying this \textit{Author's Guide}
% and the \textbf{.cls} and \textbf{.tex} files that it describes.

%
% The following two commands are all you need in the
% initial runs of your .tex file to
% produce the bibliography for the citations in your paper.
\bibliographystyle{apalike}
\bibliography{sigproc}  % sigproc.bib is the name of the Bibliography in this case
% You must have a proper ".bib" file
%  and remember to run:
% latex bibtex latex latex
% to resolve all references
%
% ACM needs 'a single self-contained file'!
%
%APPENDICES are optional
%\balancecolumns
%\appendix
%Appendix A
\balancecolumns
% That's all folks!
\end{document}
